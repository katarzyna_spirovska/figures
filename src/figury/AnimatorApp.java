package figury;

import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import java.awt.Dimension;
import java.awt.event.*;

public class AnimatorApp extends JFrame {
    static int a = 450;
    static int b = 300;
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    static public boolean a1;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    final AnimatorApp frame = new AnimatorApp();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    /**
     * Create the frame.
     * @param
     */
    public AnimatorApp() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        contentPane = new JPanel();
        
        setBounds(0, 0, a, b);
        setContentPane(contentPane);
        contentPane.setLayout(null);
        contentPane.setBackground(Color.WHITE);

        contentPane.addComponentListener(null);
        AnimPanel kanwa = new AnimPanel();
        kanwa.setBounds(0, 0, a, b);
        contentPane.add(kanwa);

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                //kanwa.setBounds(0, 0, a, b);
                kanwa.initialize();
                kanwa.buffer.setBackground(Color.WHITE);
            }
        });
        JButton btnAnimate = new JButton("Animate");
        JButton btnAdd = new JButton("Add");
        JButton btnRefresh = new JButton("Refresh");
        btnRefresh.setBounds(350, 239, 80, 23);
        contentPane.add(btnRefresh);
        btnAdd.setBounds(10, 239, 80, 23);
        contentPane.add(btnAdd);
        btnAnimate.setBounds(100, 239, 80, 23);
        contentPane.add(btnAnimate);

        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                a = contentPane.getWidth();
                b = contentPane.getHeight();

                contentPane.remove(kanwa);
                kanwa.setBounds(0, 0, a , b - 45);
                contentPane.setBounds(0, 0, a, b);
                contentPane.add(kanwa);
                kanwa.initialize();
                kanwa.buffer.setBackground(Color.WHITE);
                btnAdd.setBounds(10, b - 40, 80, 23);
                btnAnimate.setBounds(100, b - 40, 80, 23);
                btnRefresh.setBounds(a - 100,b - 40,80,23);

            }

        });

        btnAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                kanwa.addFig();
            }
        });


        btnAnimate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e)
            {
                if (a1)
                    a1 = false;
                else
                    a1 = true;
                kanwa.animate();
            }
        });

    }



}
