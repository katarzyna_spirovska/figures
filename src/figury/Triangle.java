package figury;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;

public class Triangle extends Figura {
    Triangle(Graphics2D buf, int del, int w, int h){
        super(buf, del, w, h);

        Polygon shape = new Polygon();
        shape.addPoint(0,0);
        shape.addPoint(15, 30);
        shape.addPoint(30, 0);

        aft = new AffineTransform();
        area = new Area(shape);
    }

}
