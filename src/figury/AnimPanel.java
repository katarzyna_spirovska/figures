package figury;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimPanel extends JPanel implements ActionListener {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    // bufor
    Image image;
    // wykreslacz ekranowy
    Graphics2D device;
    // wykreslacz bufora
    Graphics2D buffer;

    private int delay = 25;
    private Timer timer;
    private static int numer = 0;


    public AnimPanel() {
        super();
        setBackground(Color.WHITE);
        timer = new Timer(delay, this);
    }

    public void initialize()
    {
        int width = getWidth();
        int height = getHeight();

        image = createImage(width, height);
        buffer = (Graphics2D) image.getGraphics();
        buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        device = (Graphics2D) getGraphics();
        device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }

    Figura shapeFig ()
    {
        numer++;
        if(numer % 3 == 0){
            return new Triangle(buffer, delay, getWidth(), getHeight());
        }
        else if (numer % 2 == 0)
        {
            return  new Elipsa(buffer, delay, getWidth(), getHeight());
        }
        else {
            return new Kwadrat(buffer, delay, getWidth(), getHeight());
        }
    }

    void addFig() {
        Figura fig = shapeFig();
        timer.addActionListener(fig);
        new Thread(fig).start();
    }

    void animate() {
        if (timer.isRunning()) {
            timer.stop();

        } else {
            timer.start();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        device.drawImage(image, 0, 0, null);
        buffer.clearRect(0, 0, getWidth(), getHeight());
    }

}
